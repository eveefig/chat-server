var users = new Array();	//array of users with passwords
var rooms = new Array();	//array of room objects

//create a lobby object as the default room
var lobby = new Object();
lobby.roomName = "Lobby";
lobby.isPrivate = false
lobby.password = "";
lobby.owner = null;
lobby.users = [];
lobby.banned = [];
lobby.admin = [];
lobby.topic = "discussion";

rooms.push(lobby);

//check a username and password
function invalidUser(username, password) {
	for(var i=0; i < users.length; ++i) {
		if (users[i].user == username) {
			if (password != users[i].password) {
				return true;
			}
		}
	}
	return false;
}

//check if a roome exists
function roomExists(room_name) {
	for(var i=0; i < rooms.length; ++i) {
		if (rooms[i].roomName == room_name) {
			return true;
		}
	}
	return false;
}

//check if the username is a banned user of roomName
function userBanned(username, roomName) {
	var room = getRoom(roomName);
	for(var i=0; i < room.banned.length; ++i) {
		if (room.banned[i] == username) {
			return true;
		}
	}
	return false;
}

//get the room object in rooms with room_name
function getRoom(room_name) {
	var room;
	for(var i=0; i < rooms.length; ++i) {
		if (rooms[i].roomName == room_name) {
			room = rooms[i];
		}
	}
	
	return room;
}

//check if the user in room_name is an admin
function isAdmin(user, room_name) {
	var room = getRoom(room_name);
	
	for(var i=0; i < room.admin.length; ++i) {
		if (user == room.admin[i]) {
			return true;
		}
	}
	
	return false;
}

//returns true if curr user is the owner or an admin, and if the
//kick user is a member of the current room. Returns false if
//kickUser is the owner/admin, or if kickUser doesn't exist
//
function canKick(currUser, kickUser, roomName) {
	for(var i=0; i < rooms.length; ++i) {
		if (rooms[i].roomName == roomName) {
			
			//cant kick the owner of a chat
			if (kickUser == rooms[i].owner) {
				return false;
			}
			
			//admin cannot kick other admin
			if (isAdmin(currUser, roomName) && isAdmin(kickUser, roomName)) {
				return false
			}
			
			//if the user issuing the command is the owner or an admin
			if (rooms[i].owner == currUser || isAdmin(currUser, roomName)) {
				for(var j=0; j < rooms[i].users.length; ++j) {
					if (rooms[i].users[j] == kickUser) {
						return true;
					}
				}
			}
		}
	}
	
	return false;
}

// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");
 
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.
 
	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
 
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app.listen(3456);
 
// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.
 
	console.log("connection made");
 
	socket.on('message_to_server', function(data) {
		// This callback runs when the server receives a new message from the client.
 
		console.log("message: " + data["message"]); // log it to the Node.JS output
		io.sockets.emit("message_to_client",{
			message:data["message"],
			room:data["room"],
			sender:data["sender"]
		}); // broadcast the message to other all users	
	
	});
	
		socket.on('private_message_to_server', function(data) {
		// This callback runs when the server receives a new message from the client.
 
		console.log("private message: " + data["message"] + " to " + data["recipient"]); // log it to the Node.JS output
		io.sockets.emit("private_message_to_client",{
			message:data["message"],
			room:data["room"],
			sender:data["sender"],
			recipient:data["recipient"]
		}); // broadcast the message to other all users	
	
	});
	
	socket.on("log_in_toServer", function(data) {
		
		console.log("entered user login: " + data["username"]);
		
		//if the user exists and the password is wrong
		if (invalidUser(data['username'], data['password'])) {

			socket.emit("check_user", {success:false, message:"Cannot log in."});
		}
		else {
			var user = new Object();
			user.user = data["username"];
			user.password = data["password"];
			
			users.push(user); //add user to user array
			rooms[0].users.push(data["username"]); //add newly logged un user to the lobby
			
			socket.emit("check_user", {success:true, message:"User logged in.", username:data["username"]});
			io.sockets.emit("update_rooms", {"rooms":rooms});
		}
		
	});
	
	socket.on("logout_toServer", function(data) {
		console.log("logout: " + data["username"]);
		var room = getRoom(data["room"]);
		
		//remove the user from the group they were in
		var index = room.users.indexOf(data["username"]);
		if (index > -1) {
			room.users.splice(index, 1);
		}
		
		io.sockets.emit("update_rooms", {"rooms":rooms});
	});
	
	socket.on("leave_room", function(data) {
		console.log("leaving room: " + data["roomName"]);
		
		var room = getRoom(data["roomName"]);
			
		//remove kicked user from user array of the provided room
		var index = room.users.indexOf(data["currentUser"]);
		if (index > -1) {
			room.users.splice(index, 1);
		}
		
		//put kicked user back in the lobby
		var lobby = getRoom("Lobby");
		lobby.users.push(data["currentUser"]);
		
		socket.emit("leave_room_callback", {success:true, kickUser:data["currentUser"], roomName:data["roomName"]});
		io.sockets.emit("update_rooms", {"rooms":rooms});
		
	})
	
	socket.on("room_requested", function(data) {
		console.log("entered room_requested: " + data["roomName"] + ", " + data["password"]);
		console.log(roomExists(data["roomName"]));
		
		if (roomExists(data["roomName"])) {
			socket.emit("check_room", {success:false, message:"Room already exists"});
		}
		else{
			//create a new room object and add it to rooms array
			var room = new Object();
			room.roomName = data["roomName"];
			room.isPrivate = data["private_room"];
			room.password = data["password"];
			room.owner = data["username"];
			room.users = [];
			room.users.push(data["username"]);
			room.banned = [];
			room.admin = [];
			room.topic = "";
			
			rooms.push(room);

		
			//remove user from the lobby
			var lobby = getRoom("Lobby");
			var index = lobby.users.indexOf(data["username"]);
			if (index > -1) {
				lobby.users.splice(index, 1);
			}

			
			socket.emit("check_room", {success:true, message:"successfully created room", curr_room:data["roomName"]});
			io.sockets.emit("update_rooms", {"rooms":rooms});

		}
	});
	
	socket.on("join_room", function(data){
		console.log("entered join_room " + data["roomName"]);
		
		if (roomExists(data['roomName'])) { //if there is a room by given name to join
			var room = getRoom(data["roomName"]);
			
			if (room.isPrivate) {
				console.log("data[password]= " + data["password"] + " room.password=" + room.password);
				
				if (data["password"] != room.password) {
					socket.emit("join_room_callback", {success:false, message:"Incorrect password"});
					
					return false; //break out of function
				}
			}
			
			//if the user was banned, kick them out.
			if (userBanned(data["username"], data['roomName'])) {
				socket.emit("join_room_callback", {success:false, message:"You are banned"});
					
				return false; //break out of function
			}
			
			room.users.push(data["username"]); //add user to the users array of this room
			
			var isAnAdmin = isAdmin(data["username"], data["roomName"]);
			var isOwner = data["username"] == getRoom(data["roomName"]).owner;
			
			//remove user from the lobby
			var lobby = getRoom("Lobby");
			var index = lobby.users.indexOf(data["username"]);
			if (index > -1) {
				lobby.users.splice(index, 1);
			}

			
			socket.emit("join_room_callback", {success:true, message:"Joined room", room:data["roomName"], "isAdmin":isAnAdmin, "isOwner":isOwner});
			io.sockets.emit("update_rooms", {"rooms":rooms});
		}
		else {
			socket.emit("join_room_callback", {success:false, message:"Room doesn't exist"});
		}
	
	});
	
	socket.on("kick_user", function(data){
		console.log("kickUser: " + data["user_to_kick"] + " from " + data["currentUser"]);
		
		if(canKick(data["currentUser"], data["user_to_kick"], data["roomName"])) {
			var room = getRoom(data["roomName"]);
			
			//remove kicked user from user array of the provided room
			var index = room.users.indexOf(data["user_to_kick"]);
			if (index > -1) {
				room.users.splice(index, 1);
			}
			
			//put kicked user back in the lobby
			var lobby = getRoom("Lobby");
			lobby.users.push(data["user_to_kick"]);
			
			io.sockets.emit("kick_user_callback", {success:true, kickUser:data["user_to_kick"], roomName:data["roomName"]});
			io.sockets.emit("update_rooms", {"rooms":rooms});
		}
		else{
			socket.emit("kick_user_callback", {success:false, message:"cannot kick that user"});
		}
	});
	
	socket.on("ban_user", function(data){
		console.log("banUser: " + data["user_to_ban"]);
		if(canKick(data["currentUser"], data["user_to_ban"], data["roomName"])) {
			var room = getRoom(data["roomName"]);
			
			//remove kicked user from user array of the provided room
			var index = room.users.indexOf(data["user_to_ban"]);
			if (index > -1) {
				room.users.splice(index, 1);
			}
			room.banned.push(data['user_to_ban']); //add user to the banned list
			
			//put banned user back in the lobby
			var lobby = getRoom("Lobby");
			lobby.users.push(data["user_to_ban"]);
			
			io.sockets.emit("ban_user_callback", {success:true, bannedUser:data["user_to_ban"], roomName:data["roomName"]});
			io.sockets.emit("update_rooms", {"rooms":rooms});
		}
		else{
			socket.emit("ban_user_callback", {success:false, message:"cannot ban that user"});
		}
	});
	
	socket.on("unban_user", function(data){ 
		console.log("un-banUser: " + data["user_to_unban"]);
		
		var room = getRoom(data["roomName"]);
		
		var canUnban = false;
		
		//if the user making request is an admin or the owner
		if (data["currentUser"] == room.owner || isAdmin(data["currentUser"], data["roomName"])) {	
			
			//check if the banned user exists
			for(var i=0; i < room.banned.length; ++i) {
				if (room.banned[i] == data["user_to_unban"]) { //if the user to unban is actually banned
					canUnban = true;
				}
			}
			
			//admin cannot unban other admin, only the op can
			if (isAdmin(data["currentUser"], data["roomName"]) && isAdmin(data["user_to_unban"], data["roomName"])) {
				canUnban = false;
			}
		}
		
		if (canUnban) {
			//remove the unbanned user from the banned array
			var index = room.banned.indexOf(data["user_to_unban"]);
			if (index > -1) {
				room.banned.splice(index, 1);
			}
			
			io.sockets.emit("unban_user_callback", {success:true, unbannedUser:data["user_to_unban"], roomName:data["roomName"]});
		}
		else{
			socket.emit("unban_user_callback", {success:false, message:"cannot unban that user"});
		}

	});
	
	socket.on("make_admin", function(data) {
		console.log("make_admin: " + data["newAdmin"]);
		var room = getRoom(data["roomName"]);
		
		if (data["owner"] != room.owner) { //cant add an admin unless owner
			socket.emit("make_admin_callback", {success:false, message:"You are not the owner."})
		}
		
		//check if the user exists
		var userExists = false;
		for(var i = 0; i < room.users.length; ++i) {
			if (room.users[i] == data["newAdmin"]) {
				userExists = true;
			}
		}
		
		if (userExists) {
			room.admin.push(data["newAdmin"]); //add the new admin to the admin array
				
			io.sockets.emit("make_admin_callback", {success:true, newAdmin:data["newAdmin"], room:data["roomName"], owner:data["owner"]});
			io.sockets.emit("update_rooms", {"rooms":rooms});
		}
		else{
			socket.emit("make_admin_callback", {success:false, message:"Admin does not exist"});
		}
		
	});
	
	socket.on("remove_admin", function(data) {
		console.log("remove_admin: " + data["oldAdmin"]);
		var room = getRoom(data["roomName"]);
		
		//cant remove admin unless you are the woner
		if (data["owner"] != room.owner) {
			socket.emit("remove_admin_callback", {success:false, message:"You are not the owner."});
		}
		
		//check if the admin exists
		var adminExists = false;
		for(var i = 0; i < room.admin.length; ++i) {
			if (room.admin[i] == data["oldAdmin"]) {	//if the admin provided is indeed an admin
				adminExists = true;
			}
		}
		
		if (adminExists) {
			//remove the old admin user from the banned array
			var index = room.admin.indexOf(data["oldAdmin"]);
			if (index > -1) {
				room.admin.splice(index, 1);
			}
			
			io.sockets.emit("remove_admin_callback", {success:true, oldAdmin:data["oldAdmin"], room:data["roomName"], owner:data["owner"]});
			io.sockets.emit("update_rooms", {"rooms":rooms});
		}
		else {
			socket.emit("remove_admin_callback", {success:false, message:"Admin does not exist"});
		}
		
	});
	
	//listen for a change of topic
	socket.on("change_topic", function(data){
		var room = getRoom(data["roomName"]);
		
		//only the owner can alter the topic
		if (data["user"] == room.owner) {
			room.topic = data["topic"];
			io.sockets.emit("update_rooms", {"rooms":rooms}); //push change to all users
		}
	});
});